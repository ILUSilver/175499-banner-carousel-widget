<?php

/**-->
 * @package     BlueAcorn/BannerCarouselWidget
 * @version     0.1.0
 * @author      Gabriell J. Blue Acorn, Inc.
 * @copyright   Copyright © 2014 Blue Acorn, Inc.
 */


//block to allow the use of the custom rotation mode.

class BlueAcorn_BannerCarouselWidget_Block_Widget_Banner extends Enterprise_Banner_Block_Widget_Banner implements Mage_Widget_Block_Interface
{
    //This is the smallest amount of banners that the carousel is allowed to have and still use the carousel template.
    const MIN_NUMBER_OF_BANNERS = 2;

    //based on its parent, this grabs the 'rotate' value of the current block.
    public function getRotate()
    {
        //check if the current value of 'rotate' is carousel. if not so, run the parent method.
        return ($this->_getData('rotate') == BlueAcorn_BannerCarouselWidget_Helper_Data::CAROUSEL_IDENTIFIER) ? $this->getData('rotate') : parent::getRotate();
    }



    protected function _toHtml()
    {
        $this->_clearRenderedParams();


        if($this->getRotate() == BlueAcorn_BannerCarouselWidget_Helper_Data::CAROUSEL_IDENTIFIER &&
            count($this->getBannersContent()) >= BlueAcorn_BannerCarouselWidget_Block_Widget_Banner::MIN_NUMBER_OF_BANNERS)
        {
            //change the target template to the carousel.phtml one.
            $this->setTemplate('banner/widget/carousel.phtml');
        }

        //use mage core block template parent?
        return parent::_toHtml();
    }
}



