<?php

/**-->
 * @package     BlueAcorn/BannerCarouselWidget
 * @version     0.1.0
 * @author      Gabriell J. Blue Acorn, Inc.
 * @copyright   Copyright © 2014 Blue Acorn, Inc.
 */


//This helper is used mainly to grab the data user has set in the config and to convert sec. to ms.

class BlueAcorn_BannerCarouselWidget_Helper_Data extends Mage_Core_Helper_Abstract
{

    const CAROUSEL_IDENTIFIER = 'carousel';
    protected $_configPrefix = 'blueacorn_bannercarouselwidget/settings/';

    public function convertSecondsToMilliseconds($seconds)
    {
        $milliseconds = ($seconds * 1000);

        return $milliseconds;
    }

    //get the speed set in the system/config option for banner carousel widget.
    public function getSpeed()
    {
        return Mage::getStoreConfig($this->_configPrefix . 'speed');
    }

    //get the delay set in the system/config option for banner carousel widget. (this variable (delay) actually is used to set the 'timeout' of the carousel)
    public function getDelay()
    {
        return Mage::getStoreConfig($this->_configPrefix . 'delay');
    }

}