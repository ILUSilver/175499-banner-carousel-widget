Blue Acorn Banner Carousel Widget
=================================


[USER GUIDE LINK](https://docs.google.com/a/blueacorn.com/document/d/1j5pwoBis18WBwuY98pEJcXquanP8jPu1rh9gDirBNn4/edit#)

Blue Acorn Banner Carousel Widget is a Magento module aimed specifically(only) at the Magento Enterprise Edition version 1.14.0.
This module uses native Magento jQuery Cycle2 and allows the user to create a widget that will carousel through any number of banners that are selected by the user. There are also two configurations settings that can be set in Administrator panel ->system->config->Blue Acorn->Banner Carousel->Banner Carousel Widget, speed and delay, which will affect the carousel's attributes respectively.


Requirements:
------------
 1. Magento EE v. 1.14.0 +
 2. At least one banner.
 3. jQuery.
 4. The module DEPENDS on:
  - Mage_Widget Module  
  - Enterprise_Banner Module


Installation:
-------------
 1. In the source folder there should be one folder labeled 'App'.
 2. Make sure you have a back-up of your Magentio EE site directory.
 3. Copy the folder into your Magento EE sites root directory.
 4. A dialog box should appear, to which you should check the merge box and continue.
 5. If you would like to make any changes to the configurations settings for the module, view the next section.
 6. Congratulations, the Module is now installed onto your site.

Configuration Settings:
-----------------------
There are currently two settings for this module:
 - Located at:  Administration Panel->system->config->Blue_Acorn->Banner_Carousel->Banner_Carousel_Widget
  - *Speed:*
    - ***Description*** - This setting refer's to the amount of time it takes for the animation to complete.  
    - ***Default*** - 3
    - ***Units*** - Seconds
  - *Delay*:
    - ***Description*** - This setting refer's to the amount of time that a banner shows on screen before the animation occurs.
    - ***Default*** - 3
    - ***Units*** - Seconds

                  
Short-comings:
--------------
I am already aware of 2 issues:
 1. You can not have multiple banner carousels showing at the same time. (If you want to take a stab at it, the jQuery code located at the bottom of the .phtml contains example code that will allow it this functionality, its up to you to figure out how/where to use it.)  
 2. Unfortuantely since I opted out of using obserers in this module, I had to overwrite the layout default handle and remove the "addItem" that forced the cycle2 carousel code to run a special way so that I can change it.


Questions/Concerns/Bugs:
------------------------
If there are any, please feel free to e-mail me at: ***gabriell.juliana@blueacorn.com***  with the appropriate title in the subject line.


Module Version
==============
0.1.0

Readme Version
==============
1.0